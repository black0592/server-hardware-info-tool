// @APIVersion 1.0.0
// @Title Server Hardware API
// @Description Netadmin has every tool to get any job done, so codename for the new netadmin APIs.
// @Contact zengqi0529@163.com
package routers

import (
	"gitee.com/pippozq/server-hardware-info-tool/controllers"
	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1.0.0",
		beego.NSNamespace("/hp",
			beego.NSInclude(
				&controllers.HpController{},
			)),
	)
	beego.AddNamespace(ns)

}
