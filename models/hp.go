package models

type Processors struct {
	ProcessorFamily string `json:"ProcessorFamily"`
	Count           int64  `json:"Count"`
	Status          string `json:"Status"`
}

type Memory struct {
	Id                     string `json:"Id"`
	Name                   string `json:"Name"`
	Type                   string `json:"Type"`
	DIMMStatus             string `json:"DIMMStatus"`
	DIMMTechnology         string `json:"DIMMTechnology"`
	DIMMType               string `json:"DIMMType"`
	DataWidth              int    `json:"DataWidth"`
	TotalWidth             int    `json:"TotalWidth"`
	HPMemoryType           string `json:"HPMemoryType"`
	Manufacturer           string `json:"Manufacturer"`
	MaximumFrequencyMHz    int    `json:"MaximumFrequencyMHz"`
	MinimumVoltageVoltsX10 int    `json:"MinimumVoltageVoltsX10"`
	PartNumber             string `json:"PartNumber"`
	Rank                   int    `json:"Rank"`
	SizeMB                 int    `json:"SizeMB"`
	SocketLocator          string `json:"SocketLocator"`
}

type PhysicalPorts struct {
	FullDuplex     bool   `json:"FullDuplex"`
	MacAddress     string `json:"MacAddress"`
	GoodReceives   int    `json:"GoodReceives"`
	GoodTransmits  int    `json:"GoodTransmits"`
	StructuredName string `json:"StructuredName"`
	Type           string `json:"Type"`
}

type NetworkAdapter struct {
	Name         string          `json:"Name"`
	PhysicalPort []PhysicalPorts `json:"PhysicalPorts"`
}

type HpSession struct {
	Location  string
	AuthToken string
}

type HpServer struct {
	Host    string    `json:"host"`
	User    ILoUser   `json:"user"`
	Session HpSession `json:"-"`

	Mem        []Memory       `json:"memory"`
	Cpu        Processors     `json:"cpu"`
	NetAdapter []NetworkAdapter `json:"network_adapter"`
}

type ILoUser struct {
	Name     string `json:"UserName"`
	Password string `json:"Password"`
}

type HpReceive struct {
	Hosts      []string `json:"hosts"`
	User       ILoUser  `json:"user"`
}
